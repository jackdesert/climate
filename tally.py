from collections import namedtuple
from types import MappingProxyType
import csv
import pdb, pydoc

from comfort import Comfort


class Tally:
    """
    A Tally is the thing that adds
    """

    DAYTIME_READINGS = 'daytime_readings'
    DAYTIME_POINTS = 'daytime_points'
    DAYTIME_SCORE = 'daytime_score'

    SUMMER_DAYTIME_READINGS = 'summer_daytime_readings'
    SUMMER_DAYTIME_POINTS = 'summer_daytime_points'
    SUMMER_DAYTIME_SCORE = 'summer_score'

    EVENING_READINGS = 'evening_readings'
    EVENING_POINTS = 'evening_points'
    EVENING_SCORE = 'evening_score'

    SCORECARD_ATTRS = ('station', 'station_name', 'latitude', 'longitude', 'elevation', DAYTIME_READINGS, DAYTIME_POINTS, DAYTIME_SCORE, SUMMER_DAYTIME_READINGS, SUMMER_DAYTIME_POINTS, SUMMER_DAYTIME_SCORE, EVENING_READINGS, EVENING_POINTS, EVENING_SCORE )

    SCORECARD = namedtuple('Scorecard', SCORECARD_ATTRS)

    COMMA_SPACE = ', '

    __slots__ = (
        '_complete',
        '_filename',
        '_aggregates',
    )

    def __init__(self, filename):
        self._filename = filename
        self._complete = False
        self._aggregates = None

    def _process(self):
        if self._complete:
            return

        with open(self._filename, newline='') as handle:
            agg = self._fresh_card()
            reader = csv.DictReader(handle)
            for row in reader:
                comfort = Comfort(row)
                comfort.print()
                if not comfort.is_valid() and comfort.is_daytime():
                    continue
                agg[self.DAYTIME_READINGS] += 1
                agg[self.DAYTIME_POINTS] += comfort.value()
                if comfort.is_evening():
                    agg[self.EVENING_READINGS] += 1
                    agg[self.EVENING_POINTS] += comfort.value()
                if comfort.is_summer():
                    agg[self.SUMMER_DAYTIME_READINGS] += 1
                    agg[self.SUMMER_DAYTIME_POINTS] += comfort.value()
            agg = MappingProxyType(agg)

        # Supplemental Attributes
        supp = {}
        supp[self.DAYTIME_SCORE] = 100.0 * agg[self.DAYTIME_POINTS] / agg[self.DAYTIME_READINGS]
        supp[self.SUMMER_DAYTIME_SCORE] = 100.0 * agg[self.SUMMER_DAYTIME_POINTS] / agg[self.SUMMER_DAYTIME_READINGS]
        supp[self.EVENING_SCORE] = 100.0 * agg[self.EVENING_POINTS] / agg[self.EVENING_READINGS]
        supp.update(agg)

        self._aggregates = MappingProxyType(supp)
        self._complete = True

    def _fresh_card(self):
        return {
            self.DAYTIME_READINGS: 0,
            self.DAYTIME_POINTS: 0.0,
            self.SUMMER_DAYTIME_READINGS: 0,
            self.SUMMER_DAYTIME_POINTS: 0.0,
            self.EVENING_READINGS: 0,
            self.EVENING_POINTS: 0.0,
        }

    def _rounded_aggregates(self):
        """
        Returns self._aggregates with rounded values
        """
        output = {}
        for key, value in self._aggregates.items():
            if isinstance(value, int):
                output[key] = value
            elif isinstance(value, float):
                output[key] = round(value, 2)
            else:
                raise TypeError
        return MappingProxyType(output)



    def scorecard(self):
        self._process()
        with open(self._filename, newline='') as handle:
            reader = csv.DictReader(handle)
            for row in reader:
                station = row['STATION']
                name_raw = row['NAME']
                name_list = reversed(name_raw.split(self.COMMA_SPACE))
                name = self.COMMA_SPACE.join(name_list)
                latitude = float(row['LATITUDE'])
                longitude = float(row['LONGITUDE'])
                elevation = float(row['ELEVATION'])
                break
        data = dict(
            station=station,
            station_name=name,
            latitude=latitude,
            longitude=longitude,
            elevation=elevation,
        )
        data.update(self._rounded_aggregates())
        card = self.SCORECARD(**data)
        return card


if __name__ == '__main__':
    tal = Tally('normals/USW00094949.csv')
    card = tal.scorecard()
    pdb.set_trace()
    1
