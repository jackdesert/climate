"""
Takes a CSV file and removes certain columns
"""

import pdb, pydoc
import csv

class Prune:


    @staticmethod
    def prune(filename, cols_to_remove):
        """
        :param: filename :str: Path or string to csv file
        :param: cols_to_remove :str: A sequence of columns to remove
        """
        if not isinstance(cols_to_remove, (tuple, list)):
            raise TypeError('Expected a tuple or a list')

        cols_to_remove = set(cols_to_remove)

        with open(filename, newline='') as handle:
            reader = csv.DictReader(handle)
            original_fieldnames = reader.fieldnames
            if cols_to_remove - set(original_fieldnames):
                raise KeyError('May not remove nonexistent columns')

            fieldnames = list(original_fieldnames)
            for col in cols_to_remove:
                fieldnames.remove(col)

            output_filename = f'{filename.replace(".csv", "")}-pruned.csv'
            with open(output_filename, 'w') as output_handle:
                writer = csv.DictWriter(output_handle, fieldnames=fieldnames)
                writer.writeheader()
                for line in reader:
                    for col in cols_to_remove:
                        line.pop(col)
                    writer.writerow(line)


if __name__ == '__main__':
    remove = ('station', 'elevation', 'daytime_readings', 'daytime_points', 'summer_daytime_readings', 'summer_daytime_points', 'evening_readings', 'evening_points')
    Prune.prune('pickle/cards.csv', remove)

