
import pdb, pydoc
import csv
from datetime import datetime
import json

from jinja2 import Environment, FileSystemLoader
import os

STATIONS = []
with open('pickle/cards-pruned.csv', newline='') as handle:
    reader = csv.DictReader(handle)
    HEADERS = reader.fieldnames
    for station in reader:
        STATIONS.append(station)



# Capture our current directory
THIS_DIR = os.path.dirname(os.path.abspath(__file__))

TSTAMP = datetime.now().strftime('%f')

# Create the jinja2 environment.
# Notice the use of trim_blocks, which greatly helps control whitespace.
j2_env = Environment(loader=FileSystemLoader(THIS_DIR),
                     trim_blocks=True)
context = dict(stations=STATIONS,
        stations_json=json.dumps(STATIONS),
        headers=HEADERS,
        tstamp=TSTAMP,
        updated_at=datetime.now().strftime('%Y-%m-%dT%H:%M'),
        )
html = j2_env.get_template('templates/report.jinja2').render(**context)

with open('site/index.html', 'w') as handle:
    handle.write(html)
print('DONE')


