import pdb

from types import MappingProxyType
import pytest

from comfort import Comfort

DEFAULTS = MappingProxyType(
    {
        'STATION': 'unknown',
        'day': '1',
        'hour': '1',
        'month': '1',
        'HLY-HIDX-NORMAL': '70.0',
        'HLY-WCHL-NORMAL': '70.0',
        'HLY-CLOD-PCTBKN': 25,
        'HLY-CLOD-PCTOVC': 50,
        'HLY-CLOD-PCTSCT': 25,
    }
)

ALIASES = MappingProxyType(
    {
        'broken': 'HLY-CLOD-PCTBKN',
        'scattered': 'HLY-CLOD-PCTSCT',
        'overcast': 'HLY-CLOD-PCTOVC',
        'wind_chill': 'HLY-WCHL-NORMAL',
        'heat_index': 'HLY-HIDX-NORMAL',
    }
)

ALLOWABLE_KEYS = set(DEFAULTS.keys()).union(set(ALIASES.keys()))


def build(**params):
    if unexpected_keys := set(params.keys()) - ALLOWABLE_KEYS:
        raise KeyError(f'Unexpected keys: {unexpected_keys}')

    params_to_use = dict(DEFAULTS)
    for key, value in params.items():
        if key in DEFAULTS:
            params_to_use[key] = value
        elif key in ALIASES:
            params_to_use[ALIASES[key]] = value
        else:
            raise KeyError(key)
    return Comfort(params_to_use)


def test_cloud_cover():
    data = [
            (dict(broken=100, scattered=0, overcast=0), 0.5),
            (dict(broken=0, scattered=0, overcast=100), 1.0),
            (dict(broken=25, scattered=0, overcast=0), 0.12),
            (dict(broken=0, scattered=30, overcast=0), 0.15),
        ]
    for params, cloud_cover in data:
        comfort = build(**params)
        assert comfort.cloud_cover() == cloud_cover

def test__bound_1():
    # Format is:
    #   (value_to_check, lower_bound, upper_bound)
    data = [
            ((10, 1, 20), True),
            ((10, 10, 20), True),
            ((10, 1, 10), False),
            ((10, 1, 10.1), True),
            ]
    comfort = build()

    for arguments, output in data:
        assert comfort._bound(*arguments) == output

def test__bound_2():
    """
    When lower is greater than upper, raises exception
    """
    comfort = build()


    with pytest.raises(ValueError):
        comfort._bound(10, 100, 99)

def test_cloud_cover_1():
    comfort = build(broken=100, scattered=0, overcast=0)
    assert comfort.cloud_cover() == 0.5

def test_value_1():
    """
    When wind chill is too cold, returns 0
    """
    comfort = build(wind_chill=0)
    assert comfort.value() == 0

def test_value_2():
    """
    When heat index is too high, returns 0
    """
    comfort = build(heat_index=100)
    assert comfort.value() == 0

def test_value_3():
    """
    When heat index is too high, returns 0
    """
    comfort = build(heat_index=80)
    assert comfort.value() == 0

def test_value_4():
    """
    When heat index is medium, returns 1
    """
    comfort = build(heat_index=59)
    assert comfort.value() == 1.0

def test_value_5():
    """
    When heat index is medium high, returns  1
    """
    comfort = build(heat_index=69)
    assert comfort.value() == 1

def test_value_6():
    """
    When heat index is high, returns  fairly high
    """
    comfort = build(heat_index=75)
    assert comfort.value() == 0.75

def test_value_7():
    """
    When heat index is high and cloud level is zero, returns 0
    """
    comfort = build(heat_index=75, scattered=0, broken=0, overcast=0)
    assert comfort.value() == 0

def test_value_8():
    """
    When heat index is high and cloud level is low, returns low
    """
    comfort = build(heat_index=75, scattered=0, broken=0, overcast=10)
    assert comfort.value() == 0.1

def test_value_9():
    """
    When heat index is high and cloud level is low, returns low
    """
    comfort = build(heat_index=75, scattered=0, broken=0, overcast=10)
    assert comfort.value() == 0.1

def test__too_hot_under_any_conditions():
    """
    When 80 degrees, returns True
    """
    comfort = build(heat_index=80)
    assert comfort._too_hot_under_any_conditions()
