Climate
-------

This project analyzes climate normals. See
https://www.ncei.noaa.gov/products/us-climate-normals

- Remember, normals do not have a specific year tied to them.
  If hourly, they represent that typical hour on any given year

Docs for Climate Normals
------------------------


https://www.ncei.noaa.gov/access/search/dataset-search?text=normals

https://library.wmo.int/doc_num.php?explnum_id=4166

### Simple Text File Docs

https://www.ncei.noaa.gov/data/normals-hourly/1991-2020/doc/Readme_By-Variable_By-Station_Normals_Files.txt

### Handy Guide

    PCTCLR = Percentage occurrence of clouds clear
    PCTFEW = Percentage occurrence of clouds few
    PCTSCT = Percentage occurrence of clouds scattered
    PCTBKN = Percentage occurrence of clouds broken
    PCTOVC = Percentage occurrence of clouds overcast
    10PCTL = 10th percentile
    90PCTL = 90th percentile
    AVGSPD = Average wind speed
    PCTCLM = Percentage of calm wind occurrences
    1STDIR = Modal wind direction (1-8) in octants clockwise from north
    1STPCT = Percentage of cases from modal direction
    2NDDIR = Second mode of wind direction
    2NDPCT = Percentage of cases from second modal direction
    VCTDIR = Average wind vector direction
    VCTSPD = Magnitude of average wind vector


Install
-------

Requires python 3.8 or greater, as it uses the walrus operator

    python3 -m venv env
    source env/bin/activate
    pip install -r requirements.txt

Acquire Normals
---------------

https://www.ncei.noaa.gov/data/normals-hourly/1991-2020/

### Station Info

https://www.ncei.noaa.gov/data/normals-hourly/1991-2020/doc/hly_inventory_30yr.txt

### Documentation with Field Names

https://www.ncei.noaa.gov/data/normals-hourly/1991-2020/doc/Normals_HLY_Documentation_1991-2020.pdf

Test
----

    pytest

Recent Data #1
-----------

Start at weather.gov OR nws.noaa.gov (they are the same)

Click your state on the map.

Look at "current conditions"

(Note it uses "partly cloudy" instead of "scattered" or something else......)

Recent Data #2
--------------

Start at ncdc.noaa.gov.
Click "Data Access"
Click Quick Links
Click 1. Local Climatological
Click Local Climatalogical Data (LCD)
Choose a State from the map tool

Choose a station (Note the last four of the station ID map to the last four of the CSV station ID)

For a one-day swath, click "View Full Details".
Or for a longer period, click "Add to Cart"


Hourly Sky Conditions (From Recent Data #2)
-------------------------------------------

there is no documentation if you ask for the csv file, but there is documentation if you get the pdf.



~
~

