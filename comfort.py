"""
This module analyzes a single row of an hourly climate normal file
See https://www.ncei.noaa.gov/data/normals-hourly/1991-2020/doc/Readme_By-Variable_By-Station_Normals_Files.txt
"""


from types import MappingProxyType
import pdb


class Comfort:
    class NoMatchError(Exception):
        """
        No matching condition found
        """


    # Labels for errors
    HEAT_INDEX = 'heat_index'
    WIND_CHILL = 'wind_chill'
    CLOUDS = 'clouds'
    BLOCKING = 'blocking'


    MIN_TEMP = 40

    MAX_TEMP_WITH_NO_CLOUDS = 60
    MAX_TEMP_WITH_PART_CLOUDS = 70
    MAX_TEMP_WITH_FULL_CLOUDS = 80

    __slots__ = (
        'orig',
        'errors',
        'station',
        'day',
        'hour',
        'month',
        'heat_index',
        'clod_pctbkn',
        'clod_pctovc',
        'clod_pctsct',
        'wind_chill',
    )

    def __init__(self, row):
        self.orig = MappingProxyType(row)
        self.errors = set()

        self.station = row['STATION']
        self.day = row['day']
        self.hour = row['hour']
        self.month = row['month']

        # Temperature plus humidity effects / "feels like"
        self.heat_index = self._format_temp(
            row['HLY-HIDX-NORMAL'], self.HEAT_INDEX
        )

        # Temperature minus wind effects / "feels like"
        self.wind_chill = self._format_temp(
            row['HLY-WCHL-NORMAL'], self.WIND_CHILL
        )

        # Percent broken clouds
        self.clod_pctbkn = self._format_cloud(
            row['HLY-CLOD-PCTBKN'], self.CLOUDS
        )

        # Percent overcast clouds
        self.clod_pctovc = self._format_cloud(
            row['HLY-CLOD-PCTOVC'], self.CLOUDS
        )

        self.clod_pctsct = self._format_cloud(
            row["HLY-CLOD-PCTSCT"], self.CLOUDS
        )  # Percent scattered clouds


    def value(self):
        """
        Returns a floating point number between 0.0 and 1.0.
        1.0 is considered optimal comfort,
        and 0.0 is considered no comfort.

        :return: :float:
        """

        if self.HEAT_INDEX in self.errors:
            return 0

        # Check for too cold first, because it uses wind chill
        if self._too_cold_under_any_conditions():
            return 0.0

        if self._too_hot_under_any_conditions():
            return 0.0

        if self._comfortable_under_any_conditions():
            return 1.0

        if self.CLOUDS in self.errors:
            # Cloud information is required for the next lines to yield
            # nonzero value
            # Also marking this as a blocking error
            self.errors.add(self.BLOCKING)

            return 0

        if self._full_clouds_required():
            return self.cloud_cover()

        if self._part_clouds_required():
            # Requires at least 50% clouds for full score
            return min(2 * self.cloud_cover(), 1)

        pdb.set_trace()
        raise self.NoMatchError

    def cloud_cover(self):
        """
        A calculated value showing the overall amount of clouds
        Returns a value between 0.0 and 1.0

        :return: :float:
        """
        totals = self.clod_pctovc + 0.5 * self.clod_pctbkn + 0.5 * self.clod_pctsct
        return round(totals / 100.0, 2)

    def print(self):
        lines = [f'comfort: {self.value()}',
                 f'hour: {self.hour}',
                 f'heat_index: {self.heat_index}',
                 f'clouds: {self.cloud_cover()}',
                 ]
        if not self.is_valid():
            lines.append(f'ERRORS: {self.errors}')
        print(', '.join(lines))


    def is_valid(self):
        return self.BLOCKING not in self.errors

    def is_summer(self):
        return int(self.month) in [6,7,8]

    def is_daytime(self):
        return int(self.hour) in range(8, 20)

    def is_evening(self):
        return int(self.hour) in range(16, 20)


    def _format_cloud(self, value, message):
        value = float(value)
        if value == -9999:
            self.errors.add(message)
            return 0
        if value < 0 or value > 100:
            raise ValueError
        return value

    def _format_temp(self, value, message):
        value = float(value)
        if value == -9999:
            self.errors.add(message)
            return 0
        if value < -40 or value > 140:
            raise ValueError
        return value


    def _full_clouds_required(self):
        return self._bound(
            self.heat_index,
            self.MAX_TEMP_WITH_PART_CLOUDS,
            self.MAX_TEMP_WITH_FULL_CLOUDS,
        )

    def _part_clouds_required(self):
        return self._bound(
            self.heat_index,
            self.MAX_TEMP_WITH_NO_CLOUDS,
            self.MAX_TEMP_WITH_PART_CLOUDS,
        )

    def _too_cold_under_any_conditions(self):
        return self.wind_chill < self.MIN_TEMP

    def _too_hot_under_any_conditions(self):
        return self.heat_index >= self.MAX_TEMP_WITH_FULL_CLOUDS

    def _comfortable_under_any_conditions(self):
        return self._bound(self.heat_index, self.MIN_TEMP, self.MAX_TEMP_WITH_NO_CLOUDS)

    def _no_clouds_required(self):
        # If before sunrise or after sunset, make this return True
        if self._is_dark():
            return True
        return False

    def _is_dark(self):
        """
        TODO: add functionality that takes sunrise, sunset into consideration
        """
        return False

    def _bound(self, value, lower, upper):
        """
        Tells whether `value` is bounded by (lower, upper)
        :return: :bool:
        """
        if lower >= upper:
            raise ValueError('upper must be greater than lower')
        return lower <= value < upper
