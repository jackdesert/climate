// var used here because some browsers throw error if "let" used outside of strict context
console.log('Not seeing your changes? Make sure you transpile!');

let initializeStationsTable = () => {
    'use strict';
    // Most variables can be declared without specifying a type,
    // and typescript will infer a type. No such
    const delimiters = ['${', '}'];
    const hugeNumber = 1000000;
    let currentSortFunction;
    let currentActiveHeader;
    let mySocket;
    let stations = JSON.parse(GLOBALS.stations_json);
    const vueStationsTable = new Vue({
        delimiters: delimiters,
        el: '#stations-table-holder',
        data: {
            stations: stations
        },
        methods: {
            sortStations: function(headerId) {
                let firstValue = this.stations[0][headerId]
                // Check if value is parseable as a number
                if (isNaN(parseInt(firstValue))){
                    // Not a number
                    console.log('String Sort')
                    sortString(headerId)
                }else{
                    console.log('Numeric Sort')
                    sortNumeric(headerId)
                }
            },
            rowKlass: function (stationId, rowIndex) {
                let that = this;
                if (that.solo) {
                    return this.rowKlassWhenSolo(stationId);
                }
                let klass = '';
                if (that.selectedStationIds.includes(stationId)) {
                    klass = 'selected';
                }
                if (rowIndex % 2 === 1) {
                    klass += ' tr_stripe';
                }
                return klass;
            },
            rowKlassWhenSolo: function (stationId) {
                // This function decides whether this row should be striped
                // among its peers of other selected stations
                //
                // WARNING: This runs in N*M time
                // where N is number of stations and M is number of selected rows
                let stripe = false, that = this;
                for (let station of that.stations) {
                    if (that.selectedStationIds.includes(station.id)) {
                        stripe = !stripe;
                    }
                    if (stationId === station.id) {
                        return stripe ? 'selected tr_stripe' : 'selected';
                    }
                }
            },
            toggleSolo: function () {
                let that = this;
                if (!that.solo && (that.selectedStationIds.length === 0)) {
                    alert('Please select one or more rows first');
                    return;
                }
                that.solo = !that.solo;
            },
            highlightRow: function (stationId) {
                let that = this, index = that.selectedStationIds.indexOf(stationId), sourceElem = event.srcElement;
                if (sourceElem.href) {
                    // Do not highlight if the clicked element was a link
                    return;
                }
                if (that.solo) {
                    return;
                }
                if (index === -1) {
                    that.selectedStationIds.push(stationId);
                }
                else {
                    that.selectedStationIds.splice(index, 1);
                }
            },
            soloButtonKlass: function () {
                let that = this;
                if (that.solo) {
                    return 'solo-button solo-button_active';
                }
                else {
                    return 'solo-button';
                }
            }
        }
    });




    let target = document.getElementById('stations-tbody'),
    sortNumeric = function (attribute) {
        stations.sort(function (a, b) {
            let aa = a[attribute], bb = b[attribute];
            if (!aa) {
                aa = hugeNumber;
            }
            if (!bb) {
                bb = hugeNumber;
            }
            return bb - aa;
        });
    },
    sortString = function (attribute) {
        stations.sort(function (a, b) {
            let aa = a[attribute].toLowerCase(), bb = b[attribute].toLowerCase();
            if (aa === bb) {
                return 0;
            }
            else if (aa > bb) {
                return 1;
            }
            else {
                return -1;
            }
        });
    },


        sortByStringAttributeThenByOverallPosition = function (stringAttribute) {
        stations.sort(function (a, b) {
            let overallPositionAttribute = 'primary_rank', a1 = a[stringAttribute].toLowerCase(), b1 = b[stringAttribute].toLowerCase(), a2 = a[overallPositionAttribute], b2 = b[overallPositionAttribute];
            if (a1 > b1) {
                return 1;
            }
            else if (a1 < b1) {
                return -1;
            }
            // If you made it to here, a1 === b1
            return a2 - b2;
        });
    }, sortByCarClassThenByOverallPosition = function () {
        sortByStringAttributeThenByOverallPosition('car_class');
    }, sortByCarYearThenByCarModel = function () {
        sortByNumericThenByString('car_year', 'car_model');
    }, sortByCarModelThenByOverallPosition = function () {
        sortByStringAttributeThenByOverallPosition('car_model');
    }, sortByPaxFactorThenByOverallPosition = function () {
        sortByStringAttributeThenByOverallPosition('pax_factor');
    }, sortByClassPositionThenByOverallPosition = function () {
        stations.sort(function (a, b) {
            let A = a.class_rank * hugeNumber + a.primary_rank, B = b.class_rank * hugeNumber + b.primary_rank;
            return A - B;
        });
    }, bindHeaders = function () {
        let carClassHeader = document.getElementById('car-class'), bestCombinedHeader = document.getElementById('best-combined'), positionOverallHeader = document.getElementById('primary-rank'), positionPercentileHeader = document.getElementById('percentile-rank'), positionPaxHeader = document.getElementById('secondary-rank'), positionClassHeader = document.getElementById('class-rank'), bestCombinedPaxHeader = document.getElementById('best-combined-pax'), stationNameHeader = document.getElementById('station-name'), carYearHeader = document.getElementById('car-year'), carModelHeader = document.getElementById('car-model'), costationCarNumberHeader = document.getElementById('costation-car-number'), carNumberHeader = document.getElementById('car-number'), paxFactorHeader = document.getElementById('pax-factor'), bindings = [
            [carClassHeader, sortByCarClassThenByOverallPosition],
            [carNumberHeader, sortByCarNumber],
            [costationCarNumberHeader, sortByCostationCarNumber],
            [bestCombinedHeader, sortByOverallPosition],
            [positionOverallHeader, sortByOverallPosition],
            [positionPercentileHeader, sortByOverallPosition],
            [positionPaxHeader, sortByPaxPosition],
            [positionClassHeader, sortByClassPositionThenByOverallPosition],
            [stationNameHeader, sortByStationLastName],
            [carYearHeader, sortByCarYearThenByCarModel],
            [carModelHeader, sortByCarModelThenByOverallPosition],
            [paxFactorHeader, sortByPaxFactorThenByOverallPosition],
            [bestCombinedPaxHeader, sortByPaxPosition]
        ];
        bindings.forEach(function (array) {
            let header = array[0], func = array[1], headerAsElement = header;
            if (header === null) {
                console.log('WARNING: no header for func', func);
                return;
            }
            headerAsElement.addEventListener('click', function () {
                let that = this;
                // Store which sort function most recently selected
                currentSortFunction = func;
                currentSortFunction();
                currentActiveHeader = that;
                styleActiveHeader(that);
            });
        });
    }, styleActiveHeader = function (activeElement) {
        let sortableHeaderClass = 'sortable-header', activeHeaderClass = 'sortable-header_active', cellHighlightClass = 'td_active-sort', cellClassToHighlight = activeElement.id;
        // Header
        document.querySelectorAll('.' + sortableHeaderClass).forEach(function (element) {
            element.classList.remove(activeHeaderClass);
        });
        activeElement.classList.add(activeHeaderClass);
        // Columns
        document.querySelectorAll('td').forEach(function (element) {
            element.classList.remove(cellHighlightClass);
        });
        document.querySelectorAll('.' + cellClassToHighlight).forEach(function (element) {
            element.classList.add(cellHighlightClass);
        });
    };

    // Specify initial sort
    currentSortFunction = sortByOverallPosition;
    currentActiveHeader = document.getElementById('best-combined');
    bindHeaders();
};




//export default initializeStationsTable;
