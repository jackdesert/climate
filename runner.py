from glob import glob
from operator import itemgetter
import csv
import json
import os
import pdb, pydoc

from tally import Tally


STATION_FILES = sorted(glob('normals/*csv'))

GLOB_PATH = 'pickle/*'
CSV_PATH = 'pickle/cards.csv'

def prep():
    """
    Deletes files in pickle directory so we don't accidentally use old ones
    """
    for fname in glob(GLOB_PATH):
        os.unlink(fname)



def build_csv_from_tallies():
    """
    Builds a Tally from each station file, and collects its scorecard.
    When that is complete, writes all the cards to a single CSV file
    """
    cards = []
    for sfile in STATION_FILES:
        print(sfile)
        card = Tally(sfile).scorecard()
        cards.append(card)

    # Sorting by station_name
    cards_serializable = [card._asdict() for card in sorted(cards, key=itemgetter(1))]

    print(f'Writing to {CSV_PATH}')
    with open(CSV_PATH, 'w', newline='', encoding='utf8') as handle:
        fieldnames = cards[0]._fields
        writer = csv.DictWriter(handle, fieldnames=fieldnames)

        writer.writeheader()
        for jcard in cards_serializable:
            writer.writerow(jcard)
    print('Runner Complete')




if __name__ == '__main__':
    prep()
    build_csv_from_tallies()
