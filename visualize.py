"""
Plot the comfort as a function of heat index and clouds
https://www.statology.org/matplotlib-scatterplot-color-by-value/
"""

from types import MappingProxyType
import pdb, pydoc

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from comfort import Comfort


GRID_SIZE = 100
MARKER_SIZE = 40
DEFAULTS = MappingProxyType(
    {
        'STATION': 'visualizer',
        'day': '1',
        'hour': '12',
        'month': '1',
        'HLY-HIDX-NORMAL': '70.0',
        'HLY-WCHL-NORMAL': '70.0',
        'HLY-CLOD-PCTBKN': 0,
        'HLY-CLOD-PCTOVC': 0,
        'HLY-CLOD-PCTSCT': 0,
    }
)

ALIASES = MappingProxyType(
    {
        'overcast_percent': 'HLY-CLOD-PCTOVC',
        'heat_index': 'HLY-HIDX-NORMAL',
    }
)

ALLOWABLE_KEYS = {'overcast_percent', 'heat_index'}


def build(heat_index_, overcast_percent_):

    params_to_use = dict(DEFAULTS)

    # set wind chill and heat index to heat index
    params_to_use['HLY-HIDX-NORMAL'] = heat_index_
    params_to_use['HLY-WCHL-NORMAL'] = heat_index_
    params_to_use['HLY-CLOD-PCTOVC'] = overcast_percent_
    return Comfort(params_to_use).value()


rows = []
for heat_index in np.linspace(20, 100, GRID_SIZE):
    for overcast_percent in np.linspace(0, 100, GRID_SIZE):
        comfort = build(heat_index, overcast_percent)
        row = [heat_index, overcast_percent, comfort]
        rows.append(row)

data = np.array(rows, float)
transposed = data.transpose()

df = pd.DataFrame(
    {
        'heat_index': transposed[0],
        'overcast_percent': transposed[1],
        'comfort': transposed[2],
    }
)


plt.scatter(
    df.heat_index,
    df.overcast_percent,
    s=MARKER_SIZE,
    c=df.comfort,
    cmap='GnBu',
    marker='s', # square
)
plt.xlabel('Heat Index (degrees F)')
plt.ylabel('Overcast (%)')
plt.show()
